#see the status of VP service
/opt/teradata/viewpoint/bin/vp-control.sh status

#start the VP service
/opt/teradata/viewpoint/bin/vp-control.sh start

#stop the VP service
/opt/teradata/viewpoint/bin/vp-control.sh stop

#restart the VP service
/opt/teradata/viewpoint/bin/vp-control.sh restart

#test
wget localhost
